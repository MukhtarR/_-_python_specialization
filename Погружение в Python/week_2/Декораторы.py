from functools import wraps


def decorator(func):
    return func


@decorator  # синтаксис декоратора
def decorated():
    print('Hello!')


def logger(func):
    def wrapped(num_list):
        result = func(num_list)
        with open('log.txt', 'w') as f:
            f.write(str(result))
        return result

    return wrapped


@logger
def summator(num_list):
    return sum(num_list)


print('Summator: {}'.format(summator([1, 2, 3, 4])))


def logger(func):
    @wraps(func)
    def wrapped(*args, **kwargs):
        result = func(*args, **kwargs)
        with open('log.txt', 'w') as f:
            f.write(str(result))
        return result

    return wrapped


def logger2(filename):
    def decorator(func):
        def wrapped(*args, **kwargs):
            result = func(*args, **kwargs)
            with open(filename, 'w') as f:
                f.write(str(result))
            return result

        return wrapped

    return decorator


@logger2('new_log.txt')
def summator(num_list):
    return sum(num_list)


# без синтаксического сахара:
# summator = logger('log.txt')(summator)
summator([1, 2, 3, 4, 5, 6])
with open('new_log.txt', 'r') as f:
    print(f.read())


def first_decorator(func):
    def wrapped():
        print('Inside first_decorator product')
        return func()

    return wrapped


def second_decorator(func):
    def wrapped():
        print('Inside second_decorator product')
        return func()

    return wrapped


@first_decorator
@second_decorator
def decorated():
    print('Finally called...')


# то же самое, но без синтаксического сахара:
# decorated = first_decorator(second_decorator(decorated))
decorated()


def bold(func):
    def wrapped():
        return "<b>" + func() + "</b>"

    return wrapped


def italic(func):
    def wrapped():
        return "<i>" + func() + "</i>"

    return wrapped


@bold
@italic
def hello():
    return "hello world"


# hello = bold(italic(hello))
print(hello())
