import time


class open_file:
    def __init__(self, filename, mode):
        self.f = open(filename, mode)

    def __enter__(self):
        return self.f

    def __exit__(self, *args):
        self.f.close()


class timer:
    def __init__(self):
        self.start = time.time()

    def current_time(self):
        return time.time() - self.start

    def __enter__(self):
        return self

    def __exit__(self, *args):
        print(f'Elapsed time: {self.current_time()}')


with timer() as t:
    time.sleep(1)
    print(f'Current time: {t.current_time()}')
    time.sleep(1)
